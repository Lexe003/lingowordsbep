package nl.benhadi.lingowordsbep.lingoword.exception;

public class InvalidLingoWord extends RuntimeException {
    public InvalidLingoWord(String message) {
        super(message);
    }

    public static InvalidLingoWord invalidWord(String word) {
        return new InvalidLingoWord(
                String.format(
                        "Invalid lingo word: {0} - Only characters allowed with min-max length: 5-7 characters", word
                )
        );
    }

}
