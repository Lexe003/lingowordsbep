package nl.benhadi.lingowordsbep.lingoword.domain;

import nl.benhadi.lingowordsbep.lingoword.exception.InvalidLingoWord;

import java.util.Objects;

public class LingoWord {
    private String word;

    public LingoWord(String word) {
        if (!isValidWord(word)) {
            throw InvalidLingoWord.invalidWord(word);
        }

        this.word = word;
    }

    public String getName() {
        return word;
    }

    public void setName(String name) {
        if (!isValidWord(name)) {
            throw InvalidLingoWord.invalidWord(word);
        }
        this.word = name;
    }

    public boolean isValidWord(String word) {
        return word.matches("^[a-z]{5,7}$");
    }

    @Override
    public String toString() {
        return "LingoWord{" +
                "word='" + word + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LingoWord lingoWord = (LingoWord) o;
        return Objects.equals(word, lingoWord.word);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word);
    }
}
