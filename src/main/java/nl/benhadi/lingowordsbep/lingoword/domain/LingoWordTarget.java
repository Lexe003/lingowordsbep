package nl.benhadi.lingowordsbep.lingoword.domain;

import java.util.List;

public interface LingoWordTarget {
    void exportLingoWords(List<LingoWord> lingoWords);
}
