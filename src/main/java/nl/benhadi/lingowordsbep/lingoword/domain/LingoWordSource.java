package nl.benhadi.lingowordsbep.lingoword.domain;

import java.util.List;

public interface LingoWordSource {
    List<LingoWord> getAllLingoWords();
}
