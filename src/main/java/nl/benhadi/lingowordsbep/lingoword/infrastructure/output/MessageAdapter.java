package nl.benhadi.lingowordsbep.lingoword.infrastructure.output;

import nl.benhadi.lingowordsbep.lingoword.domain.LingoWord;

import java.util.List;

public interface MessageAdapter {
    void publishMessage(List<LingoWord> lingoWords);
}
