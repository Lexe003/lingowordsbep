package nl.benhadi.lingowordsbep.lingoword.infrastructure.input;

import nl.benhadi.lingowordsbep.lingoword.domain.LingoWord;

import java.util.List;

public interface LingoWordDeserializer {

    List<LingoWord> deserializeFile(String filePath);

}
