package nl.benhadi.lingowordsbep.lingoword.infrastructure.output;

import nl.benhadi.lingowordsbep.lingoword.domain.LingoWord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class KafkaMessageAdapter implements MessageAdapter {

    @Value("${kafka.topic}")
    private String topic;

    private final KafkaTemplate<String, List<LingoWord>> kafkaTemplate;

    public KafkaMessageAdapter(KafkaTemplate<String, List<LingoWord>> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public void publishMessage(List<LingoWord> lingoWords) {
        Message<List<LingoWord>> message = MessageBuilder
                .withPayload(lingoWords)
                .setHeader(KafkaHeaders.TOPIC, topic)
                .build();

        kafkaTemplate.send(message);
    }
}
