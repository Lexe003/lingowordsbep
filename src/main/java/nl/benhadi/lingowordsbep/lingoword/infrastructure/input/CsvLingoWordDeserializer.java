package nl.benhadi.lingowordsbep.lingoword.infrastructure.input;

import com.opencsv.CSVReader;
import nl.benhadi.lingowordsbep.lingoword.domain.LingoWord;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class CsvLingoWordDeserializer implements LingoWordDeserializer {
    @Override
    public List<LingoWord> deserializeFile(String filePath) {

        List<LingoWord> allWords = new ArrayList<>();
        try (InputStream inputStream = getClass().getResourceAsStream(filePath);
             CSVReader reader = new CSVReader(new InputStreamReader(inputStream))) {

            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                if (nextLine[0].matches("^[a-z]{5,7}$")) {
                    allWords.add(new LingoWord(nextLine[0]));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allWords;
    }
}
