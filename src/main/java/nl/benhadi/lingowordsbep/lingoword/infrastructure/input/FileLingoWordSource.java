package nl.benhadi.lingowordsbep.lingoword.infrastructure.input;

import nl.benhadi.lingowordsbep.lingoword.domain.LingoWord;
import nl.benhadi.lingowordsbep.lingoword.domain.LingoWordSource;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FileLingoWordSource implements LingoWordSource {

    private final static String FILE_PATH = "/words.csv";

    private final LingoWordDeserializer lingoWordDeserializer;

    public FileLingoWordSource(LingoWordDeserializer lingoWordDeserializer) {
        this.lingoWordDeserializer = lingoWordDeserializer;
    }

    @Override
    public List<LingoWord> getAllLingoWords() {
        return lingoWordDeserializer.deserializeFile(FILE_PATH);
    }
}
