package nl.benhadi.lingowordsbep.lingoword.infrastructure.output;

import nl.benhadi.lingowordsbep.lingoword.domain.LingoWord;
import nl.benhadi.lingowordsbep.lingoword.domain.LingoWordTarget;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MessageLingoWordTarget implements LingoWordTarget {

    private final MessageAdapter messageAdapter;

    public MessageLingoWordTarget(MessageAdapter messageAdapter) {
        this.messageAdapter = messageAdapter;
    }

    @Override
    public void exportLingoWords(List<LingoWord> lingoWords) {
        messageAdapter.publishMessage(lingoWords);
    }
}
