package nl.benhadi.lingowordsbep.lingoword.application;

import nl.benhadi.lingowordsbep.lingoword.domain.LingoWord;
import nl.benhadi.lingowordsbep.lingoword.domain.LingoWordSource;
import nl.benhadi.lingowordsbep.lingoword.domain.LingoWordTarget;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LingoWordProcessor {

    private final LingoWordSource lingoWordSource;
    private final LingoWordTarget lingoWordTarget;

    public LingoWordProcessor(LingoWordSource lingoWordSource, LingoWordTarget lingoWordTarget) {
        this.lingoWordSource = lingoWordSource;
        this.lingoWordTarget = lingoWordTarget;
    }

    public void initializeLingoWords() {
        List<LingoWord> lingoWords = lingoWordSource.getAllLingoWords();
        lingoWordTarget.exportLingoWords(lingoWords);
    }
}
