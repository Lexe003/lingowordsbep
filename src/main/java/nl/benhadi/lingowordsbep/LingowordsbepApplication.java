package nl.benhadi.lingowordsbep;

import nl.benhadi.lingowordsbep.lingoword.application.LingoWordProcessor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LingowordsbepApplication implements CommandLineRunner {

	private final LingoWordProcessor lingoWordProcessor;
	public LingowordsbepApplication(LingoWordProcessor lingoWordProcessor) {
		this.lingoWordProcessor = lingoWordProcessor;
	}

	public static void main(String[] args) {
		SpringApplication.run(LingowordsbepApplication.class, args);
	}

	@Override
	public void run(String... args) {
		lingoWordProcessor.initializeLingoWords();
	}
}
