package nl.benhadi.lingowordsbep.lingoword.infrastructure.output;

import nl.benhadi.lingowordsbep.lingoword.domain.LingoWord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.Message;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class KafkaMessageAdapterTest {

    @Mock
    private KafkaTemplate<String, List<LingoWord>> mockKafkaTemplate;

    private KafkaMessageAdapter kafkaMessageAdapterUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        kafkaMessageAdapterUnderTest = new KafkaMessageAdapter(mockKafkaTemplate);
    }

    @Test
    void testPublishMessage() {
        final List<LingoWord> lingoWords = Collections.singletonList(new LingoWord("woord"));
        when(mockKafkaTemplate.send(any(Message.class))).thenReturn(null);

        kafkaMessageAdapterUnderTest.publishMessage(lingoWords);
        verify(mockKafkaTemplate).send(any(Message.class));
    }
}
