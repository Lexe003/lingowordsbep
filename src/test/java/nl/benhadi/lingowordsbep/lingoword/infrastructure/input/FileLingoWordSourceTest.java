package nl.benhadi.lingowordsbep.lingoword.infrastructure.input;

import nl.benhadi.lingowordsbep.lingoword.domain.LingoWord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class FileLingoWordSourceTest {

    @Mock
    private LingoWordDeserializer mockLingoWordDeserializer;

    private FileLingoWordSource fileLingoWordSourceUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        fileLingoWordSourceUnderTest = new FileLingoWordSource(mockLingoWordDeserializer);
    }

    @Test
    void testGetAllLingoWords() {
        final List<LingoWord> expectedResult = Arrays.asList(new LingoWord("woorden"), new LingoWord("vogels"), new LingoWord("praag"));
        when(mockLingoWordDeserializer.deserializeFile("/words.csv")).thenReturn(Arrays.asList(new LingoWord("woorden"), new LingoWord("vogels"), new LingoWord("praag")));
        final List<LingoWord> result = fileLingoWordSourceUnderTest.getAllLingoWords();

        assertEquals(expectedResult, result);
    }
}
