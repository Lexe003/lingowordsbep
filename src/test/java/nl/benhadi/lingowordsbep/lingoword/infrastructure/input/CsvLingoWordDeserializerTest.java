package nl.benhadi.lingowordsbep.lingoword.infrastructure.input;

import nl.benhadi.lingowordsbep.lingoword.domain.LingoWord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.constraints.Null;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CsvLingoWordDeserializerTest {

    private CsvLingoWordDeserializer csvLingoWordDeserializerUnderTest;

    @BeforeEach
    void setUp() {
        csvLingoWordDeserializerUnderTest = new CsvLingoWordDeserializer();
    }

    @Test
    void testDeserializeFile() {
        final List<LingoWord> expectedResult = Arrays.asList(new LingoWord("woorden"), new LingoWord("vogels"), new LingoWord("praag"), new LingoWord("vogel"));
        final List<LingoWord> result = csvLingoWordDeserializerUnderTest.deserializeFile("/words.csv");

        assertEquals(4, result.size());
        assertEquals(expectedResult, result);
    }

    @Test
    void itShouldThrowNullPointerExceptionWhenFileNotFound() {
        assertThrows(NullPointerException.class,
                () -> {
                    csvLingoWordDeserializerUnderTest.deserializeFile("/invalidfile.csv");
                });
    }
}
