package nl.benhadi.lingowordsbep.lingoword.infrastructure.output;

import nl.benhadi.lingowordsbep.lingoword.domain.LingoWord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

class MessageLingoWordTargetTest {

    @Mock
    private MessageAdapter mockMessageAdapter;

    private MessageLingoWordTarget messageLingoWordTargetUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        messageLingoWordTargetUnderTest = new MessageLingoWordTarget(mockMessageAdapter);
    }

    @Test
    void testExportLingoWords() {
        final List<LingoWord> lingoWords = Arrays.asList(new LingoWord("woord"));
        messageLingoWordTargetUnderTest.exportLingoWords(lingoWords);

        verify(mockMessageAdapter).publishMessage(Arrays.asList(new LingoWord("woord")));
    }
}
