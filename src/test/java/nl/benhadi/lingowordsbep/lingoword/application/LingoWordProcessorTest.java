package nl.benhadi.lingowordsbep.lingoword.application;

import nl.benhadi.lingowordsbep.lingoword.domain.LingoWord;
import nl.benhadi.lingowordsbep.lingoword.domain.LingoWordSource;
import nl.benhadi.lingowordsbep.lingoword.domain.LingoWordTarget;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Arrays;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class LingoWordProcessorTest {

    @Mock
    private LingoWordSource mockLingoWordSource;
    @Mock
    private LingoWordTarget mockLingoWordTarget;

    private LingoWordProcessor lingoWordProcessorUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        lingoWordProcessorUnderTest = new LingoWordProcessor(mockLingoWordSource, mockLingoWordTarget);
    }

    @Test
    void testInitializeLingoWords() {
        when(mockLingoWordSource.getAllLingoWords()).thenReturn(Arrays.asList(new LingoWord("woord")));
        lingoWordProcessorUnderTest.initializeLingoWords();

        verify(mockLingoWordTarget).exportLingoWords(Arrays.asList(new LingoWord("woord")));
    }
}
