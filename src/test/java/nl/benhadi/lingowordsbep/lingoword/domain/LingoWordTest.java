package nl.benhadi.lingowordsbep.lingoword.domain;

import nl.benhadi.lingowordsbep.lingoword.exception.InvalidLingoWord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class LingoWordTest {

    private LingoWord lingoWordUnderTest;

    @BeforeEach
    void setUp() {
        lingoWordUnderTest = new LingoWord("woord");
    }

    @Test
    void testGetName() {
        final String result = lingoWordUnderTest.getName();

        assertEquals("woord", result);
    }

    public static Stream<Arguments> validWordExamples() {
        return Stream.of(
                Arguments.of("abcde", true),
                Arguments.of("abcdef", true),
                Arguments.of("abcdefg", true)
        );
    }

    public static Stream<Arguments> invalidWordExamples() {
        return Stream.of(
                Arguments.of("a", false),
                Arguments.of("ab", false),
                Arguments.of("abc", false),
                Arguments.of("abcd", false),
                Arguments.of("1wooord", false),
                Arguments.of("asdasdasdasd", false),
                Arguments.of("$23423232", false),
                Arguments.of("cascasdasda", false),
                Arguments.of("12345", false)
                );
    }

    @ParameterizedTest
    @MethodSource({ "validWordExamples", "invalidWordExamples" })
    void testWordValidation(String word, Boolean expectedResult) {
        final boolean actualResult = lingoWordUnderTest.isValidWord(word);
        assertEquals(actualResult, expectedResult);
    }

    @ParameterizedTest
    @MethodSource({ "validWordExamples", "invalidWordExamples" })
    void testSetName(String word, Boolean result) {

        if (result) {
            lingoWordUnderTest.setName(word);
            assertEquals(lingoWordUnderTest.getName(), word);
        } else {
            assertThrows(InvalidLingoWord.class,
                    () -> lingoWordUnderTest.setName(word));
        }

    }


    @ParameterizedTest
    @MethodSource({ "validWordExamples", "invalidWordExamples" })
    void testConstructor(String word, Boolean isValid) {
        if(!isValid) {
            assertThrows(InvalidLingoWord.class,
                    () -> new LingoWord(word));
        } else {
            LingoWord lingoWord = new LingoWord(word);
            assertEquals(lingoWord.getName(), word);
        }
    }

    @Test
    void testToString() {
        final String result = lingoWordUnderTest.toString();

        assertEquals("LingoWord{word='woord'}", result);
    }

    @Test
    void testEquals() {
        final boolean isNotEqual = lingoWordUnderTest.equals("o");
        assertFalse(isNotEqual);

        LingoWord copy = lingoWordUnderTest;
        final boolean isEqual = lingoWordUnderTest.equals(copy);
        assertTrue(isEqual);

        Object o = null;
        final boolean isNullAndClassNotEqual = lingoWordUnderTest.equals(o);
        assertFalse(isNullAndClassNotEqual);
    }

    @Test
    void testHashCode() {
        final int result = lingoWordUnderTest.hashCode();

        assertEquals(113316136, result);
    }
}
