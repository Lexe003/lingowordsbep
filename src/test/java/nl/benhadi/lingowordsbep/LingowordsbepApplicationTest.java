package nl.benhadi.lingowordsbep;

import nl.benhadi.lingowordsbep.lingoword.application.LingoWordProcessor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

class LingowordsbepApplicationTest {

    @Mock
    private LingoWordProcessor mockLingoWordProcessor;

    private LingowordsbepApplication lingowordsbepApplicationUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
        lingowordsbepApplicationUnderTest = new LingowordsbepApplication(mockLingoWordProcessor);
    }

    @Test
    void testRun() {
        lingowordsbepApplicationUnderTest.run("args");
        verify(mockLingoWordProcessor).initializeLingoWords();
    }
}
